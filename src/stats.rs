//! Port of Sedgewicks Java StdStats.java https://introcs.cs.princeton.edu/java/22library/StdStats.java.html

/// Returns the maximum value in the specified array. If NaN is encountered,
/// the resulting value will also be NaN.
///
/// ```edition2018
///    let hasNaN : &[f64] = &[ 3.4, -2.3, 8.7, f64::NAN ];
///    assert_eq!(*max(hasNaN), f64::NAN);
///
///    let empty : &[f64] = &[];
///    assert_eq!(*max(empty), f64::NEG_INFINITY);
///
///    let hasMin : &[f64] = &[ -2.3, 3.4, 0.1243 ];
///    assert_eq!(*max(hasMin), 3.4);
///    
///    let nanNegInf : &[f64] = &[ f64::NEG_INFINITY, f64::NAN ];
///    assert_eq!(*max(nanNegInf), f64::NAN);
/// ```
pub(crate) fn max<'a>(a: &'a [f64]) -> &'a f64 {
    // Sedgewicks implementation defaults to negative infinity,
    // using an Option<> or Result<> is probably more rust like
    let mut max = &f64::NEG_INFINITY;

    for i in a {
        if i.is_nan() {
            return &f64::NAN;
        } else if *i > *max {
            max = i;
        }
    }

    max
}

/// Returns the maximum value in the specified subarray. In Rust
/// this method is redundant as utilizing slices with the double()
/// function is sufficient, but it is provided for completeness.
/// Note the range is inclusive on the lower bound but not the upper.
///
/// ```edition2018
///  let slice : &[f64] = &[ f64::NAN, -2.3, 1.0, 0.5, f64::NEG_INFINITY ];
///  assert_eq!(*max(slice, 1, 4), -2.3);
/// ```
pub(crate) fn max<'a>(a: &'a [f64], lo: u32, hi: u32) -> &'a f64 {
    max(a[lo..hi])
}

///
pub(crate) fn min<'a>(a: &'a [f64]) -> &'a f64 {
    let min : f64 = &f64::NEG_INFINITY;


}

///
pub(crate) fn min(a: &'a [f64], lo: u32, hi: u32) -> &'a f64 {
}

///
pub(crate) fn mean(a: &'a [f64]) -> &'a f64 {
}

///
pub(crate) fn var() -> f64 {
}

///
pub(crate) fn stddev() -> f64 {
}

///
pub(crate) fn median() -> f64 {
}

#[cfg(test)]
mod test {

    #[test]
    fn test() {
    }
}
