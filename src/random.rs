//! Port of Sedgewicks StdRandom.java
//! https://introcs.cs.princeton.edu/java/22library/StdRandom.java.html

///
pub fn setSeed() {
}

///
pub fn uniform() {
}

///
pub fn bernoulli() {
}

///
pub fn guassian() {
}

///
pub fn gaussian() {
}

///
pub fn discrete() {
}

///
pub fn shuffle() {
}

#[cfg(test)]
mod test {

    #[test]
    fn test() {
    }
}
